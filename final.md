# Title 1

## Title 2

| h1 | h2 |
|----|:--:|
| a  | 234 |
| b  | 3 |

Esto es texto normal
que sigue en la misma línea.

Esto son  
dos líneas distintas.

```bash
$ ls -lh # Código
```

![alternativo](./images/imagen.png)

Esto es una lista

sin orden:

+ uno
+ dos
    + tres
    + cuatro
    
con orden

1. uno
1. dos
1. tres

[//]: <> (Esto es un comentario c:)

